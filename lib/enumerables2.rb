require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.length == 0
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)

  long_strings.all?{|el| el.include?(substring)  }
end


# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  string.gsub!(" ","")
   string.split("").select do |el|
     string.count(el) > 1
   end.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
    clean_string = string.delete(".,:!?")
    clean_string.split(" ").sort_by{ |el| el.length }.reverse.slice(0..1).reverse
end
# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ("a".."z").reject do |el|
    string.include?(el)
  end
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |el| not_repeat_year?(el)   }
end

def not_repeat_year?(year)
  year.to_s.split("").all?{ |el|  year.to_s.count(el) < 2}
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  #1 only stayed on the chart for a week at a time
  #2

  songs.select do |el,idx|
       no_repeats?(el,songs) == true
  end.uniq

end

def no_repeats?(song_name, songs)
  songs.each_with_index do |el,idx|
   if el == songs[idx+1] and el == song_name
     return false
  end
end
true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  clean_string = string.delete(".,;:!?").split(" ").select{|el| el.include?("c")}.join(" ")
  longest = c_distance(clean_string.split(" ").sort_by{|el|  c_distance(el)}.first)
  clean_string.split(" ").select { |el| c_distance(el) == longest
  }.first

end

def c_distance(word)
  distance = 0
  distance = word.length - 1

  word.split("").each_with_index do |el,idx|
    if word.length - 1 - idx < distance and el == "c"
      distance = word.length - 1 - idx
    end
  end
  return distance
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  result = []
  start_index = nil
  end_index = nil

  arr.each_with_index do |el,idx|
      if el == arr[idx + 1]
        if start_index == nil
         start_index = idx
        end
      else
         if start_index != nil
            end_index = idx
            result.push([start_index,end_index])
           start_index = nil
         end


      end
  end

  return result
end
